DROP DATABASE if EXISTS todo;

CREATE DATABASE todo;

USE todo;

CREATE TABLE user (
    id INT PRIMARY KEY auto_increment,
    username VARCHAR(30) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    firstname VARCHAR(100),
    lastname VARCHAR(100)
);

CREATE TABLE task (
    id INT PRIMARY KEY auto_increment,
    title VARCHAR(255) NOT NULL,
    added TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    description TEXT,
    user_id INT NOT NULL,
    index (user_id),
    FOREIGN KEY (user_id) REFERENCES user(id)
    ON DELETE RESTRICT
)